package directorybuilder;

import java.io.File;

public class Builder {
    public void printTree(String fiePath) {
        File root = new File(fiePath);
        printTree(root);
    }

    private void printTree(File f) {
        if(!f.isDirectory()) {
            System.out.println(" - " + f.getName());
            return;
        }
        System.out.println(f.getName());
        String[] names = f.list();
        if(names == null||names.length==0) {
            return;
        }

        File[] files = f.listFiles();
        for (int i = 0; i < files.length; i++) {
            printTree(files[i]);
        }
    }
}
