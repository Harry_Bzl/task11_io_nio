package readwriteperformance;

import java.io.*;
import java.util.Date;

public class Comparing {
    public static void main(String[] args) {
        File fileIn = new File("src\\main\\resources\\book.pdf");
        File fileOut = new File("src\\main\\resources\\BookOut.pdf");
        System.out.println("File size is " + fileOut.length());
        System.out.println("File size (int) is " + (int)fileOut.length());

        System.out.println("Test read all bytes and wright");
        testReadAllLines(fileIn, fileOut);
        System.out.println("Test read ");
        testRead(fileIn);
    }

    private static void testReadAllLines(File fileIn, File fileOut) {
        long start;
        long time;
        try (InputStream is = new FileInputStream(fileIn);
             OutputStream os = new FileOutputStream(fileOut)) {
            start = new Date().getTime();
            byte [] bytes = is.readAllBytes();
            time = new Date().getTime() - start;
            System.out.println("Total time for reading without buffer " + time);
            os.write(bytes);
            time = new Date().getTime() - start;
            System.out.println("Total time for writing without buffer " + time);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(fileIn), 100);
             BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(fileOut), 100)) {
            start = new Date().getTime();
            byte [] bytes = bis.readAllBytes();
            time = new Date().getTime() - start;
            System.out.println("Total time for reading with buffer of size 100 " + time);
            bos.write(bytes);
            time = new Date().getTime() - start;
            System.out.println("Total time for writing with buffer of size 100 " + time);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(fileIn), 500);
             BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(fileOut), 500)) {
            start = new Date().getTime();
            byte [] bytes = bis.readAllBytes();
            time = new Date().getTime() - start;
            System.out.println("Total time for reading with buffer of size 500 " + time);
            bos.write(bytes);
            time = new Date().getTime() - start;
            System.out.println("Total time for writing with buffer of size 500 " + time);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void testRead(File fileIn) {
        long start;
        long time;
        int read;
        try (InputStream is = new FileInputStream(fileIn)){
            start = new Date().getTime();
            do {
                read = is.read();
//                System.out.println(read);
            }
            while (read >= 0 );
            time = new Date().getTime() - start;
            System.out.println("Total time for reading without buffer " + time);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(fileIn), 100)) {
            start = new Date().getTime();
            do {
                read = bis.read();
            }
            while (read >= 0 );
            time = new Date().getTime() - start;
            System.out.println("Total time for reading with buffer of size 100 " + time);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(fileIn), 500)) {
            start = new Date().getTime();
            do {
                read = bis.read();
            }
            while (read >= 0 );
            time = new Date().getTime() - start;
            System.out.println("Total time for reading with buffer of size 500 " + time);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
