package comments;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class FindComments {
    private File file;

    FindComments(File file) {
        this.file = file;
    }

    private List<String> findComments (){
        List <String> comments = new ArrayList<>();
        List <String> lines = new ArrayList<>();
        try {
            lines = Files.readAllLines(file.toPath()).stream().map(String::trim).collect(Collectors.toList());
        } catch (IOException e) {
            System.out.println("No such file");
        }

        for (String line:lines){
            if (line.contains("//")) {
                comments.add(line.substring(line.indexOf("//")));
            }
            else if (line.startsWith("/*") || line.startsWith("*")) comments.add(line);
        }
        return comments;
    }
    void printComments() {
        List<String> comments = this.findComments();
        System.out.println("Comments of file " + file.getName() +  " are: ");
        for (String i: comments) System.out.println(i);
    }
}
