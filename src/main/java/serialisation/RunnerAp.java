package serialisation;

import serialisation.model.Group;
import serialisation.model.Student;
import serialisation.model.Teacher;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class RunnerAp {
    public static void main(String[] args) {
        Student st1 = new Student("Garry", 18, "Math");
        Student st2 = new Student("Joe", 19, "Biology");
        List <Student> students = new ArrayList<Student>();
        students.add(st1);
        students.add(st2);
        Teacher teacher = new Teacher("Mike", 40, "IT");
        Group group = new Group();
        group.setFaculty("IT");
        group.setGroupId(21);
        group.setStudents(students);
        group.setTeacher(teacher);

        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("src\\main\\resources\\group.dat")))
        {
            System.out.println(group);
            oos.writeObject(group);
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }

        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("src\\main\\resources\\group.dat")))
        {
            Group g=(Group) ois.readObject();
            System.out.println(g);
        }
        catch(Exception ex){

            System.out.println(ex.getMessage());
        }
    }
}
