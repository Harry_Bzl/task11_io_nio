package serialisation.model;

public class Student extends Person {
    private String faculty;
    public Student(String name, int age, String faculty) {
        super(name,age);
        this.faculty = faculty;
    }

    @Override
    public String toString() {
        return "Student{" + "name " + this.getName() + " age " + this.getAge() +
                "faculty='" + faculty + '\'' +
                '}';
    }
}
