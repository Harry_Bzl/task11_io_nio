package serialisation.model;

public class Teacher extends Person {

    private  String discipline;

    public Teacher(String name, int age, String discipline) {
        super(name, age);
        this.discipline = discipline;
    }

    @Override
    public String toString() {
        return "Teacher{" + "name " + this.getName() + " age " + this.getAge() +
                "discipline='" + discipline + '\'' +
                '}';
    }
}
