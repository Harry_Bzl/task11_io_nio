package serialisation.model;

import java.io.Serializable;
import java.util.List;

/**
 * @autor Harry
 * @version 1.1 from 09.11.18
 * class Group description (comment 1)
 */

public class Group implements Serializable {
    private List<Student> students;
    private Teacher teacher;
    private transient int groupId;
    private String faculty;
/* get sudent list (comment 2) */
    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students; // set students(coment3)
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    @Override
    public String toString() {
        return "Group{" +
                "students=" + students +
                ", teacher=" + teacher +
                ", groupId=" + groupId +
                ", faculty='" + faculty + '\'' +
                '}';
    }
}
